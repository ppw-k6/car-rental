from django.db import models
from datetime import datetime, date
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth import get_user_model


rating_choice = [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]
# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profileImage = models.URLField()

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
    
class Category(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name
        
class Car(models.Model):
    carName = models.CharField(max_length=100)
    owner = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete = models.CASCADE)
    year = models.CharField(max_length=4)
    city = models.CharField(max_length=30)
    price = models.IntegerField()
    description = models.CharField(max_length=100)

    def __str__(self):
        return self.carName
#Nambah model ini
class FavCar(models.Model):
    car = models.ForeignKey(Car, on_delete = models.CASCADE, primary_key = False)
    person = models.ForeignKey( User, on_delete = models.CASCADE, primary_key = False)
    

    def __str__(self):
        return self.car.carName

class ArticleModel(models.Model):
    title = models.CharField(max_length = 50)
    date =  models.DateField()
    content = models.CharField(max_length = 10000)
    rating = models.IntegerField(default=0)
    oldrating= models.IntegerField(default=0)
    totalrating=models.IntegerField(default=0)

    def __str__(self):
        return self.title

class ReviewModel(models.Model):
    title = models.CharField(max_length = 50)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    carChoice = Car.objects.all()
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    content = models.CharField(max_length = 10000)
    rating_choice = [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]
    rating = models.CharField(max_length=255, choices=rating_choice, default='NULL')
    startDate = models.DateField()
    endDate = models.DateField()
    dateCreated = models.DateTimeField('Created Time', auto_now_add=True, null=True)

    def __str__(self):
        return self.title

class TransactionModel(models.Model):
    username = models.CharField(max_length = 50)
    car = models.CharField(max_length = 50)
    date = models.DateTimeField('Created Time', auto_now_add=True, null=True)
    startDate = models.DateField()
    endDate = models.DateField()
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return self.username