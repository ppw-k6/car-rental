from django.forms import  TextInput, Select
from django import forms
from django.db import models
from django.forms import Select
from .models import Car, ReviewModel, TransactionModel, Profile, ArticleModel
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your forms here.
class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    image = forms.URLField(required=True, help_text='Enter image url.')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2', 'image')


class AddCar_Form(forms.ModelForm):
    class Meta:
        model = Car
        fields=['carName','owner',
        'category','year','price','city',
        'description']


class SearchCategory_Form(forms.ModelForm):
    class Meta:
        model = Car
        fields = [
            'category'
        ]
    
    def __init__(self, *args, **kwargs):
        super(SearchCategory_Form, self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = "Select a Car Type"
        # following line needed to refresh widget copy of choice list
        self.fields['category'].widget.choices = self.fields['category'].choices

class SearchResult_Form(forms.ModelForm):
    class Meta:
        model = Car
        fields = [
            'carName', 'owner', 'city', 'year', 'price' 
        ]

class DateInput(forms.DateInput):
    input_type = 'date'

class ReviewForm(forms.ModelForm):
    class Meta:
        model = ReviewModel
        exclude = ('user',)
        fields = ['title', 'car', 'content', 'rating', 'startDate', 'endDate']
        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control', 'id': 'title'}),
            'car':forms.Select(attrs={'class':'form-control', 'id': 'car'}),
            'content':forms.TextInput(attrs={'class':'form-control', 'id': 'content'}),
            'rating':forms.Select(attrs={'class':'form-control', 'id': 'rating'}),
            'startDate':DateInput(attrs={'id': 'startDate'}),
            'endDate':DateInput(attrs={'id': 'endDate'})
        }

class ArticleForm(forms.ModelForm):
    class Meta:
        model=ArticleModel
        fields= ['title','date','content']
        widgets={
            'title':forms.TextInput(attrs={'class':'form-control'}),
            'date':DateInput(),
            'content':forms.TextInput(attrs={'class':'form-control'})
            #'rating':forms.Select(attrs={'class':'form-control'})
        }        

class Transaction_Form(forms.ModelForm):
    class Meta:
        model = TransactionModel
        fields = ['username', 'car', 'startDate', 'endDate']
        widgets = {
            'username':forms.TextInput(attrs={'class':'form-control'}),
            'car':forms.TextInput(attrs={'class':'form-control'}),
            'startDate':DateInput(),
            'endDate':DateInput()
        }
