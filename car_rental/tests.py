from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.contrib.auth.models import User
from .apps import CarRentalConfig
from .forms import *
from .views import *
from .models import *

# Create your tests here.
class carRentalUnitTests(TestCase):
    def testHompageURLExists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def testAddCarRentURLRedirect(self):
        response = Client().get('/add-car')
        self.assertEqual(response.status_code, 302)
    
    def testRentURLRedirect(self):
        response = Client().get('/rent')
        self.assertEqual(response.status_code, 302)

    def testReviewsURLRedirect(self):
        response = Client().get('/reviews')
        self.assertEqual(response.status_code, 302)
    
    def testArticleURLRedirect(self):
        response = Client().get('/article')
        self.assertEqual(response.status_code, 302)

    def testTransactionsURLRedirect(self):
        response = Client().get('/transactions')
        self.assertEqual(response.status_code, 302)

    
    
    def testRentDetailsURLExists(self):
        response = Client().get('/rent-details')
        self.assertEqual(response.status_code, 200)
    
    def test_login_page_url_exists(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)
    
    def test_signup_page_url_exists(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_search_api_url_exists(self):
        response = Client().get('/api/search')
        self.assertEqual(response.status_code, 200)

# --------------------------------------------------------------------------------------------------------------------------------------------    
    
    def testHomepageTemplate(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def testAddCarTemplate(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/add-car')
        
        self.assertTemplateUsed(response, 'add_car.html')
    
    def testArticleTemplate(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/article')
        self.assertTemplateUsed(response, 'article.html')
    
    def testRentDetailsTemplate(self):
        response = Client().get('/rent-details')
        self.assertTemplateUsed(response, 'rent_details.html')

    def testRentTemplate(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/rent')
        self.assertTemplateUsed(response, 'rent.html')
    
    def testReviewsTemplate(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/reviews')
        self.assertTemplateUsed(response, 'reviews.html')
    
    def testTransactionsTemplate(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/transactions')
        self.assertTemplateUsed(response, 'transactions.html')
    
    def test_login_template_used(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_signup_template_used(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'signup.html')

# --------------------------------------------------------------------------------------------------------------------------------------------

    def testHomepageFunction(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def testAddCarFunction(self):
        found = resolve('/add-car')
        self.assertEqual(found.func, add_car)

    def testArticleFunction(self):
        found = resolve('/article')
        self.assertEqual(found.func, article)
    
    def testRentDetailsFunction(self):
        found = resolve('/rent-details')
        self.assertEqual(found.func, rent_details)
    
    def testRentFunction(self):
        found = resolve('/rent')
        self.assertEqual(found.func, rent)
    
    def testReviewsFunction(self):
        found = resolve('/reviews')
        self.assertEqual(found.func, reviews)
    
    def testTransactionsFunction(self):
        found = resolve('/transactions')
        self.assertEqual(found.func, transactions)
    
    def test_login_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, log_in)
    
    def test_logout_func(self):
        found = resolve('/logout')
        self.assertEqual(found.func, log_out)

    def test_search_func(self):
        found = resolve('/api/search')
        self.assertEqual(found.func, search)
        
    def testSubmitReviewFunction(self):
        found = resolve('/submit-review')
        self.assertEqual(found.func, submit_review)

    def testDeleteReviewFunction(self):
        found = resolve('/delete-review')
        self.assertEqual(found.func, delete_review)
        
# --------------------------------------------------------------------------------------------------------------------------------------------

    def testApp(self):
        self.assertEqual(CarRentalConfig.name, 'car_rental')

# --------------------------------------------------------------------------------------------------------------------------------------------

    def testHomepageGreeting(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Welcome to ", response_content)

    def testHomepageMotto(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Rent a car at best rates.", response_content)

# --------------------------------------------------------------------------------------------------------------------------------------------

    def test_create_new_category(self):
        new = Category.objects.create(name = 'SUV')
        self.assertTrue(isinstance(new, Category))
        self.assertTrue(new.__str__(), new.name)
        count_category = Category.objects.all().count()
        self.assertEqual(count_category, 1)

    def test_create_new_car(self):
        cat = Category.objects.create(name = 'SUV')
        new = Car.objects.create(carName = 'Car', owner = 'Jon', category = cat, year = '2017', city = 'Jakarta', price = 5, description = 'Gud')
        self.assertTrue(isinstance(new, Car))
        self.assertTrue(new.__str__(), new.carName)
        count_car = Car.objects.all().count()
        self.assertEqual(count_car, 1)

    def test_create_new_article(self):
        new = ArticleModel.objects.create(title = 'Article', date = '2018-10-22', content = 'Gud',rating=0,oldrating=0,totalrating=0)
        self.assertTrue(isinstance(new, ArticleModel))
        self.assertTrue(new.__str__(), new.title)
        count_article = ArticleModel.objects.all().count()
        self.assertEqual(count_article, 1)

    def test_create_new_review(self):
        self.user = User.objects.create_user(username='andrew', password='fasilkom123')
        car = Category.objects.create(name = 'SUV')
        newCar = Car.objects.create(carName = 'Car', owner = 'Jon', category = car, year = '2017', city = 'Jakarta', price = 5, description = 'Gud')
        new = ReviewModel.objects.create(title='review title', user = self.user, car = newCar, content = 'Gud', rating = '1', startDate = '2018-10-22', endDate = '2018-10-24')
        self.assertTrue(isinstance(new, ReviewModel))
        self.assertTrue(new.__str__(), new.title)
        count_review = ReviewModel.objects.all().count()
        self.assertEqual(count_review, 1)

    def test_create_new_transaction(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        cat = Category.objects.create(name = 'SUV')
        newCar = Car.objects.create(carName = 'Car', owner = 'Jon', category = cat, year = '2017', city = 'Jakarta', price = 5, description = 'Gud')
        new = TransactionModel.objects.create(username = 'Rafa', car = newCar, startDate = '2018-10-22', endDate = '2018-10-24', user = user)
        self.assertTrue(isinstance(new, TransactionModel))
        self.assertTrue(new.__str__(), new.username)
        count_transaction = TransactionModel.objects.all().count()
        self.assertEqual(count_transaction, 1)

    def test_create_new_fav_car(self):
        user = User.objects.create(username="Test")
        cat = Category.objects.create(name = 'SUV')
        newCar = Car.objects.create(carName = 'Car', owner = 'Jon', category = cat, year = '2017', city = 'Jakarta', price = 5, description = 'Gud')
        new = FavCar.objects.create(car = newCar, person = user)
        self.assertTrue(isinstance(new, FavCar))
        self.assertTrue(new.__str__(), newCar.carName)
        count_favCar = FavCar.objects.all().count()
        self.assertEqual(count_favCar, 1)


# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
    
    def test_sign_up(self):
        client = Client()
        response = client.post('/signup', {'first_name' : 'ayaya', 'last_name' : 'clap', 'username' : 'AYAYA', 'email' : 'email@email.com', 'password1' : 'fasilkom123', 'password2' : 'fasilkom123', 'image' : 'https://assets.change.org/photos/4/ff/rw/VbFFrWROepQkXJr-1600x900-noPad.jpg?1528245321'})
        self.assertEqual(response.status_code, 302)


    def test_log_out(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = Client().get("/logout")
        self.assertEqual(response.status_code, 302)
    
    def test_post_login_user_exist(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        response = Client().post("/login", {'username' : 'test_user', 'password' : 'fasilkom123'})
        self.assertEqual(response.status_code, 302)

    def test_post_login_user_not_exist(self):
        response = Client().post("/login", {'username' : 'test_user', 'password' : 'fasilkom123'})
        self.assertTemplateUsed(response, 'login.html')


    def test_profile_page_valid(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/profile')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page_invalid(self):
        client = Client()
        response = client.get('/profile')
        self.assertEqual(response.status_code, 302)


    def test_invalid_post(self):
        Client().post('/add-car', {'carName':'','owner':'','category':'','year':'','city':'','price':'','description':''})
        response = Car.objects.all().count()
        self.assertEqual(response, 0)

    def test_form_new_car(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        testCategory = Category.objects.create(name = 'SUV').pk
        client.post('/add-car', {'carName':'1','owner':'2','category':testCategory,'year':'8','city':'4','price':5,'description':'67'})
        response = Car.objects.all().count()
        self.assertEqual(response, 1)

    def test_form_new_car_invalid(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        testCategory = Category.objects.create(name = 'SUV').pk
        client.post('/add-car', {'carName':'1','owner':'2','category':testCategory,'year':'8','city':'4','price':'abc','description':'67'})
        response = Car.objects.all().count()
        self.assertEqual(response, 0)

    def test_rent_details_receive_post_request_search(self):
        testCategory = Category.objects.create(name = 'SUV')
        cars = Car.objects.create(carName = '1',owner = '2',category = testCategory,year = '8',city = '4',price = 5,description = '67')
        response = Client().post('/rent-details', {'search':'Rent','carName':'d', 'owner':'d', 'city':'d', 'year':'d', 'price':5, 'description':'d'}).context['cars'].count()
        self.assertEqual(response, 0)

    def test_rent_details_receive_post_request_transaction(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        dates = date(year = 2020, month = 8, day = 12)
        client.post('/rent-details',{'transaction':'Rent','car':'c', 'username':'u', 'startDate':'2018-10-22', 'endDate':'2018-10-22'})
        response = TransactionModel.objects.all().count()
        self.assertEqual(response, 1)

    def test_submit_review(self):
        self.maxDiff = None
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        testCategory = Category.objects.create(name = 'SUV')
        cars = Car.objects.create(carName = '1',owner = '2',category = testCategory,year = '8',city = '4',price = 5,description = '67')
        rating = ReviewModel.rating_choice[1]
        response = client.post('/submit-review', {'title':'review title','car':cars,'content':'d','rating':rating,'startDate':'2018-10-22','endDate':'2018-10-22'})
        reviews = ReviewModel.objects.all()
        results=[]
        for review in reviews:
            createdCasted = str(dateparse.parse_datetime(str(review.dateCreated)).strftime('%B %d, %Y, %I:%M %p'))
            startCasted = str(review.startDate.strftime('%B %d, %Y'))
            endCasted = str(review.endDate.strftime('%B %d, %Y'))
            results.append({'id': review.id, 'title': review.title, 'user': review.user.username, 'car': review.car.carName, 'content': review.content, 'rating': review.rating, 'startDate': startCasted, 'endDate': endCasted, 'dateCreated': createdCasted})
        json_obj = JsonResponse({'newReviews': results})
        self.assertEqual(response.content, json_obj.content)
        

    def test_fav_car_post_request(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        testCategory = Category.objects.create(name = 'SUV')
        car = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        
        client.post('/favourite-car', {'id' : car.id})
        response = FavCar.objects.all().count()
        self.assertEqual(response, 1)

    def test_unfav_car_get_request(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.get('/unfavourite-car')
        response = FavCar.objects.all().count()
        self.assertEqual(response,0)
#---------------------------------------------------------------------------------------------------
    def test_article_post(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response = client.post('/article', {'title' : 'title', 'date' :'2018-10-22', 'content': 'ayaya'})
        self.assertEquals(response.status_code, 302)

    def test_rating(self):
        newArticle=ArticleModel.objects.create(title = 'Article', date = '2018-10-22', content = 'Gud',rating=0,oldrating=0,totalrating=0)
        newArticle.save()
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response=client.get('/update-rating',{'title':'Article','rating_value':'5'})
        self.assertEqual(response.status_code,200)
        

    def test_unfav_car_post_request(self):
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        testCategory = Category.objects.create(name = 'SUV')
        car = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        fav = FavCar.objects.create(car = car, person = user)
        client.post('/unfavourite-car', {'fav' : fav.id})
        response = FavCar.objects.all().count()
        self.assertEqual(response,0)
        

    def test_rating_post(self):
        newArticle=ArticleModel.objects.create(title = 'Article', date = '2018-10-22', content = 'Gud',rating=0,oldrating=0,totalrating=0)
        newArticle.save()
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')
        response=client.post('/update-rating',{'title':'Article','rating_value':'5'})
        self.assertContains(response,"Request method is not a GET")
        

    def test_unfav_car_post_request_non_authenticated(self):
        response = Client().post('/unfavourite-car', {'car_name' : 'car', 'owner': 'owner'})
        self.assertTemplateUsed(response, 'homepage.html')


    def test_search_with_no_result(self):
        testCategory = Category.objects.create(name = 'SUV')
        Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        response = Client().get('/api/search', {'keyword': 'no', 'check': 'manual'})
        self.assertJSONEqual(response.content, {"cars": 0})

    def test_search_with_car_name(self):
        testCategory = Category.objects.create(name = 'SUV')
        testCar = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        response = Client().get('/api/search', {'keyword': 'car', 'check': 'manual'})
        json_obj = JsonResponse({'cars': list(Car.objects.filter(carName__icontains='car').values())})
        self.assertEqual(response.content, json_obj.content)

    def test_search_with_price_less_or_equal(self):
        testCategory = Category.objects.create(name = 'SUV')
        testCar = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        response = Client().get('/api/search', {'keyword': '6', 'check': 'manual'})
        json_obj = JsonResponse({'cars': list(Car.objects.filter(price__lte='6').values())})
        self.assertEqual(response.content, json_obj.content)

    def test_search_with_category_name(self):
        testCategory = Category.objects.create(name = 'SUV')
        testCar = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        response = Client().get('/api/search', {'keyword': 1, 'check': 'category'})
        json_obj = JsonResponse({'cars': list(Car.objects.filter(category=1).values())})
        self.assertEqual(response.content, json_obj.content)
    
    def test_search_with_keyword_all(self):
        testCategory = Category.objects.create(name = 'SUV')
        testCar = Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
        response = Client().get('/api/search', {'keyword': 'All', 'check': 'manual'})
        json_obj = JsonResponse({'cars': list(Car.objects.all().values())})
        self.assertEqual(response.content, json_obj.content)

    def test_delete_review(self):
        self.maxDiff = None
        user = User.objects.create(username='test_user')
        user.set_password('fasilkom123')
        user.save()
        client = Client()
        client.login(username='test_user', password = 'fasilkom123')

        car = Category.objects.create(name = 'SUV')
        newCar = Car.objects.create(carName = 'Car', owner = 'Jon', category = car, year = '2017', city = 'Jakarta', price = 5, description = 'Gud')
        new = ReviewModel.objects.create(title='review1', user = user, car = newCar, content = 'Gud', rating = '1', startDate = '2018-10-22', endDate = '2018-10-24')
        new2 = ReviewModel.objects.create(title='review2', user = user, car = newCar, content = 'Mantap', rating = '5', startDate = '2018-10-25', endDate = '2018-10-26')

        response = client.post('/delete-review', {'reviewId':new.id, 'reviewUser':user})
        reviews = ReviewModel.objects.all()
        results=[]
        for review in reviews:
            createdCasted = str(dateparse.parse_datetime(str(review.dateCreated)).strftime('%B %d, %Y, %I:%M %p'))
            startCasted = str(review.startDate.strftime('%B %d, %Y'))
            endCasted = str(review.endDate.strftime('%B %d, %Y'))
            results.append({'id': review.id, 'title': review.title, 'user': review.user.username, 'car': review.car.carName, 'content': review.content, 'rating': review.rating, 'startDate': startCasted, 'endDate': endCasted, 'dateCreated': createdCasted})
        json_obj = JsonResponse({'newReviews': results})
        self.assertEqual(response.content, json_obj.content)
    

# class FunctionalTest(LiveServerTestCase):
#     def setUp(self) :
#         chrome_options = Options()
#         #chrome_options.add_argument('--dns-prefetch-disable')
#         #chrome_options.add_argument('--no-sandbox')
#         #chrome_options.add_argument('--headless')        
#         chrome_options.add_argument('disable-gpu')
#         self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.driver.quit()
#         super(FunctionalTest,self).tearDown()
    
    # def test_login(self):
    #     self.driver.get(self.live_server_url)
    #     response_page = self.driver.page_source

    #     self.assertIn("Welcome", response_page)
    #     time.sleep(5)
    #     first = 'test'
    #     last = 'name'
    #     username = 'test'
    #     email = 'test@test.com'
    #     passw = 'signup123'
    #     image = 'https://picsum.photos/id/237/200/300'

    #     # Sign Up
    #     self.driver.find_element_by_name('dropAcc').click()
    #     self.driver.find_element_by_id('signUp').click()
    #     time.sleep(3)

    #     self.driver.find_element_by_id('id_first_name').send_keys(first)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_last_name').send_keys(last)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_username').send_keys(username)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_email').send_keys(email)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_password1').send_keys(passw)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_password2').send_keys(passw)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('id_image').send_keys(image)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('signUpBut').click()
    #     time.sleep(3)

    #     # Log in
    #     self.driver.find_element_by_name('username').send_keys(username)
    #     time.sleep(3)
    #     self.driver.find_element_by_name('password').send_keys(passw)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('logInBut').click()
    #     time.sleep(3)

    #     response_page = self.driver.page_source
    #     self.assertIn('Welcome', response_page)

    #     #Log out
    #     self.driver.find_element_by_name('dropAcc').click()
    #     self.driver.find_element_by_id('logOut').click()
    #     time.sleep(3)

    #     response_page = self.driver.page_source
    #     self.assertIn("Welcome", response_page)

    #     # Login with wrong username and password
    #     self.driver.find_element_by_name('dropAcc').click()
    #     self.driver.find_element_by_id('logIn').click()
    #     time.sleep(3)

    #     self.driver.find_element_by_name('username').send_keys(passw)
    #     time.sleep(3)
    #     self.driver.find_element_by_name('password').send_keys(username)
    #     time.sleep(3)
    #     self.driver.find_element_by_id('logInBut').click()
    #     time.sleep(3)

    #     response_page = self.driver.page_source
    #     self.assertIn("Invalid username or password", response_page)

    #def test_search_using_category(self):
    #    self.browser.get('http://localhost:8000/rent')
    #    self.assertIn('Rent', self.browser.title)
    #    self.assertIn('rent', self.browser.find_element_by_tag_name('h1').text)
    #    self.assertIn('search', self.browser.find_element_by_class_name('carousel-item').text)
    #    time.sleep(2)

    #    categorySelect = self.browser.find_element_by_xpath("//select[@name='category']")
    #    categorySelect.click()
    #    time.sleep(5)
    #    searchButton = self.browser.find_element_by_id('categorySearch')
    #    searchButton.click()
    #    time.sleep(5)

    #    self.assertIn('search', self.browser.find_element_by_class_name('carousel-item').text)
    #    rightButton = self.browser.find_element_by_class_name('carousel-control-next')
    #    rightButton.click()
    #    time.sleep(2)

    #    self.assertIn('car', self.browser.find_element_by_tag_name('h6').text)

    #def test_search_using_car_name(self):
    #    testCategory = Category.objects.create(name = 'SUV')
    #    Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
    #    self.browser.get('http://localhost:8000/rent')
    #    self.assertIn('Rent', self.browser.title)
    #    self.assertIn('rent', self.browser.find_element_by_tag_name('h1').text)
    #    self.assertIn('search', self.browser.find_element_by_class_name('carousel-item').text)
    #    time.sleep(2)

    #def test_search_using_price(self):
    #    testCategory = Category.objects.create(name = 'SUV')
    #    Car.objects.create(carName = 'car',owner = 'owner',category = testCategory,year = '2012',city = 'city',price = 5,description = '67')
    #    self.browser.get('http://localhost:8000/rent')
    #    self.assertIn('Rent', self.browser.title)
    #    self.assertIn('rent', self.browser.find_element_by_tag_name('h1').text)
    #    self.assertIn('search', self.browser.find_element_by_class_name('carousel-item').text)
    #    time.sleep(2)
