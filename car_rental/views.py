from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .forms import AddCar_Form, SearchCategory_Form, ReviewForm, SearchResult_Form, Transaction_Form, SignUpForm, ArticleForm
from .models import Car, ArticleModel, ReviewModel, TransactionModel, Profile, FavCar
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.http import JsonResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import dateparse
import json
import math

# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')
    

def profile_page(request):
    if(request.user.is_authenticated):
        current_user = request.user
        favCarList = FavCar.objects.filter(person = current_user)
        reviewList = ReviewModel.objects.filter(user= current_user)
        transactionList  = TransactionModel.objects.filter(user = current_user)
        response = {'car' : favCarList, 'reviewList' : reviewList, 'transactions' : transactionList}
        return render(request, 'profile.html', response)
    return redirect('/login')

def log_in(request):
    
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        user_dict = {'username':username, 'password':password}
        if user is not None:
            login(request, user)
            return redirect('/')
        
        else:
            message = {'message':'Invalid username or password.'}
            return render(request, 'login.html', message)
    
    else :
        return render(request, 'login.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.profileImage = form.cleaned_data.get('image')
            user.save()
            return redirect('/login')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})

def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('/')

def add_car(request):
    if(request.user.is_authenticated):
        addCarForm = AddCar_Form()
        if (request.method=='POST'):
            addCarForm = AddCar_Form(request.POST)
            if(addCarForm.is_valid()):
                addCarForm.save()
                return HttpResponseRedirect('add-car')
            else:
                addCarForm = AddCar_Form()
        return render(request, 'add_car.html',{'form': addCarForm})
    return redirect('/login')

def rent(request):
    if(request.user.is_authenticated):
        searchForm = SearchCategory_Form()
        return render(request, 'rent.html', {'form' : searchForm})
    return redirect('/login')

def search(request):
    cars = 0
    if request.method == 'GET':
        if 'keyword' in request.GET and 'check' in request.GET:
            keyword = request.GET['keyword']
            if request.GET['check'] == 'category':
                if Car.objects.filter(category=keyword).exists():
                    cars = list(Car.objects.filter(category=keyword).values())
            elif request.GET['check'] == 'manual':
                if keyword.lower() == 'all':
                    cars = list(Car.objects.all().values())
                elif Car.objects.filter(carName__icontains=keyword).exists():
                    cars = list(Car.objects.filter(carName__icontains=keyword).values())
                elif keyword.isdigit():
                    if Car.objects.filter(price__lte=keyword).exists():
                        cars = list(Car.objects.filter(price__lte=keyword).values())   
    return JsonResponse({'cars': cars})

def rent_details(request):
    resultForm = SearchResult_Form()
    transactionForm = Transaction_Form()
    cars = Car.objects.all()
    if request.method == 'POST':
        if 'transaction' in request.POST:
            transactionForm = Transaction_Form(request.POST)
            transactionForm.car = request.POST.get('car')
            
            if transactionForm.is_valid():
                transaction = transactionForm.save(commit=False)
                transaction.user = request.user
                transactionForm.save()
                return HttpResponseRedirect('transactions')
        elif 'search' in request.POST:
            searchForm = SearchResult_Form(request.POST)
            if searchForm.is_valid():
                resultName = searchForm.cleaned_data['carName']
                resultOwner = searchForm.cleaned_data['owner']
                resultCity = searchForm.cleaned_data['city']
                resultYear = searchForm.cleaned_data['year']
                resultPrice = searchForm.cleaned_data['price']
                cars = cars.filter(carName = resultName, owner = resultOwner, city = resultCity, year = resultYear, price = resultPrice)
    return render(request, 'rent_details.html', {'cars' : cars, 'form' : transactionForm})

def fav_car(request):
    if request.method == "POST":
        carId = request.POST['id']
        car = Car.objects.get(id = carId)
        current_user = request.user
        if not FavCar.objects.filter(person = current_user, car = car).exists():
            new = FavCar()
            new.car = car
            new.person = current_user
            new.save()
    return HttpResponse('')

def unfav_car(request):
    if(request.user.is_authenticated):
        if request.method == "POST":
            fav = request.POST['fav']
            person  = request.user
            car = FavCar.objects.get(id = fav)
            car.delete()
            return redirect("/profile")
        person  = request.user
        favCarList = FavCar.objects.filter(person = person)
        response = {'car' : favCarList}
        return render(request, 'profile.html', response)
    return render(request, 'homepage.html')


def transactions(request):
    if(request.user.is_authenticated):
        response = TransactionModel.objects.all().order_by('-date')
        return render(request, 'transactions.html', {'transactions' : response})
    return redirect('/login')


def reviews(request):
    if(request.user.is_authenticated):
        form = ReviewForm()
        reviewList = ReviewModel.objects.all()
        response = {
            'form' : form,
            'reviewList' : reviewList
        }
        return render(request, 'reviews.html', response)
    return redirect('/login')


def article(request):
    if(request.user.is_authenticated):
        form= ArticleForm()
        if (request.method=='POST'):
            form = ArticleForm(request.POST)
            if(form.is_valid()):
                form.save()
                return HttpResponseRedirect('article')
        articleList = ArticleModel.objects.all()
        response = {
            'articleList' : articleList,
            'form': form
        }
        return render(request, 'article.html', response)
    return redirect('/login')



@csrf_exempt
def submit_review(request):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.user = request.user
            form.save()

            reviews = ReviewModel.objects.all()
            results=[]
            for review in reviews:
                createdCasted = str(dateparse.parse_datetime(str(review.dateCreated)).strftime('%B %d, %Y, %I:%M %p'))
                startCasted = str(review.startDate.strftime('%B %d, %Y'))
                endCasted = str(review.endDate.strftime('%B %d, %Y'))
                results.append({'id': review.id, 'title': review.title, 'user': review.user.username, 'car': review.car.carName, 'content': review.content, 'rating': review.rating, 'startDate': startCasted, 'endDate': endCasted, 'dateCreated': createdCasted})

    return JsonResponse({'newReviews': results})

@csrf_exempt
def delete_review(request):
    user = request.user
    if request.method == 'POST':
        if (user.username == request.POST['reviewUser']):
            review = ReviewModel.objects.get(id = request.POST['reviewId'])
            review.delete()

            reviews = ReviewModel.objects.all()
            results=[]
            for review in reviews:
                createdCasted = str(dateparse.parse_datetime(str(review.dateCreated)).strftime('%B %d, %Y, %I:%M %p'))
                startCasted = str(review.startDate.strftime('%B %d, %Y'))
                endCasted = str(review.endDate.strftime('%B %d, %Y'))
                results.append({'id': review.id, 'title': review.title, 'user': review.user.username, 'car': review.car.carName, 'content': review.content, 'rating': review.rating, 'startDate': startCasted, 'endDate': endCasted, 'dateCreated': createdCasted})

    return JsonResponse({'newReviews': results})
    
def update_rating(request):

    if request.method == 'GET':
        titlestring = request.GET['title']
        ratingstring= request.GET['rating_value']
        article = ArticleModel.objects.get(title=titlestring) 
        article.rating += 1
        temp=article.totalrating
        calculation= ((temp+int(ratingstring))/article.rating)
        article.totalrating+= int(ratingstring)
        article.oldrating=math.floor(calculation)

        article.save()
        return HttpResponse(article.oldrating) 
    return HttpResponse("Request method is not a GET")
     
    

