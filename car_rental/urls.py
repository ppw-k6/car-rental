from django.urls import path
from . import views


urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('add-car', views.add_car, name='add_car'),
    path('rent', views.rent, name='rent'),
    path('rent-details', views.rent_details, name='rent_details'),
    path('transactions', views.transactions, name='transactions'),
    path('reviews', views.reviews, name='reviews'),
    path('article', views.article, name='article'),
    path('login', views.log_in, name='log_in'),
    path('logout', views.log_out, name='log_out'),
    path('signup', views.signup, name='signup'),
    path('profile', views.profile_page, name='profile_page'),
    path('favourite-car', views.fav_car, name = 'favourite-car'),
    path('unfavourite-car', views.unfav_car, name = 'unfavourite-car'),
    path('api/search', views.search, name='search'),
    path('submit-review', views.submit_review, name = "submit_review"),
    path('delete-review', views.delete_review, name = "delete_review"),
    path('update-rating',views.update_rating,name='update_rating')

]