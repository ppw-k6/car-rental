from django.contrib import admin
from .models import ArticleModel, ReviewModel, Car, Category, TransactionModel, Profile, FavCar

# Register your models here.
admin.site.register(ArticleModel)
admin.site.register(ReviewModel)
admin.site.register(Car)
admin.site.register(Category)
admin.site.register(TransactionModel)
admin.site.register(Profile)
admin.site.register(FavCar)
