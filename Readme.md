<h1>GROUP - K6</h1>
<h2>CASE STUDY 2</h2>

<h3>MEMBER:</h3>

<ul>
    <li>Anggrio Wildanhadi Sutopo - 1806173544</li>
    <li>Nabila Edina - 1806173531</li>
    <li>Muhammad Rafadana Mountheira - 1806241135</li>
    <li>Rayhan Muzakki - 1806241204</li>
    <li>Oliver M Fadhlurrahman - 1806241154</li>
</ul>


<h3>Link:</h3>

Gitlab Repository:
https://gitlab.com/ppw-k6/car-rental

Heroku:
https://wdp-groupki6.herokuapp.com/

Wireframe:
https://wireframe.cc/TcosMC

Prototype/Mockup (Figma):
https://www.figma.com/proto/bgTfMFGclJkjkknO96PNjn/Andrew's?node-id=33%3A47&scaling=min-zoom

<h3>Description:</h3>
    The application that we are going to make is "Andrew's Car Rental" User are able to add their cars for rent, search, and view a list of cars that can be rented out. The cars are categorized into car types and user who have already rented the car can make a review that will be displayed on the car description page.

<h3>Features:</h3>
<ul>
    <li>View articles</li>
    <li>Create review</li>
    <li>View user created reviews</li>
    <li>Add cars for rental</li>
    <li>View & Rent available cars</li>
    <li>View transaction history</li>
</ul>

    


